<?php

/**
 * @file mandrill_async.module
 *
 * Adds an option to take advantage of Mandrill's native asynchronous feature
 * to send emails, thereby reducing load on the Drupal server.
 */

/**
 * Implements hook_form_FORMID_alter().
 */
function mandrill_async_form_mandrill_admin_settings_alter(&$form, &$form_state, $form_id) {
  if (!empty($form['asynchronous_options'])) {
    $form['asynchronous_options']['mandrill_native_async'] = array(
      '#type' => 'checkbox',
      '#default_value' => variable_get('mandrill_native_async', FALSE),
      '#title' => t("Use Mandrill's native asynchronous send option"),
      '#description' => t("Reduces your website's server load. May be used with or without the cron queue option below."),
      '#weight' => -1,
    );
  }
}

/**
 * Implements hook_mandrill_mail_alter().
 *
 * Changes the mail sending function to the asynchronous version below, if
 * requested.
 */
function mandrill_async_mandrill_mail_alter(&$mandrill_params, &$message) {
  if (variable_get('mandrill_native_async')) {
    $mandrill_params['function'] = 'mandrill_async_sender_plain';
  }
}

/**
 * The actual function that calls the Mandrill API send function asynchronously.
 *
 * @param array $message
 *   Associative array containing message data.
 *
 * @return array
 *   Results of sending the message asynchronously.
 *
 * @throws Mandrill_Error
 */
function mandrill_async_sender_plain($message) {
  if (!($mailer = mandrill_get_api_object())) {
    throw new Mandrill_Error('Missing API key.');
  }
  return $mailer->messages->send($message, $async = TRUE);
}